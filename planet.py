from Tkinter import *

class Planet:
  def __init__(self, x, y, vx, vy):
    self.x = x
    self.y = y
  
    self.vx = vx
    self.vy = vy
  
    self.ms = 1.99 * 10 ** 16
    self.gamma = 6.67 * 10 ** -11
  
  def r(self, x, y):
    return (x ** 2 + y ** 2) ** 0.5

  def advance(self, dt):
    ax = -1 * self.gamma * self.ms * (self.x / self.r(self.x, self.y) ** 3)
    ay = -1 * self.gamma * self.ms * (self.y / self.r(self.x, self.y) ** 3)

    print("ax: " + str(ax))
    print("ay: " + str(ay))
    
    self.vx = self.vx + ax * dt * 0.5
    self.vy = self.vy + ay * dt * 0.5
    
    print("vx: " + str(self.vx))
    print("vy: " + str(self.vy))
    
    self.x = self.x + self.vx * dt
    self.y = self.y + self.vy * dt
    
    print("x: " + str(self.x))
    print("y: " + str(self.y))

erde = Planet(149.6, 0, 0, 30)
mars = Planet(228, 0, 0, 20)


canvasWidth = 500
canvasHeight = canvasWidth

def cadvance():
  i = 0
  while i < 20:
    erde.advance(0.01)
    mars.advance(0.01)
    i += 1

  canvas.create_oval(erde.x - 5 + canvasHeight * 0.5,
		     erde.y + 5 + canvasHeight * 0.5,
		     erde.x + 5 + canvasHeight * 0.5,
		     erde.y - 5 + canvasHeight * 0.5)

  canvas.create_oval(mars.x - 5 + canvasHeight * 0.5,
		    mars.y + 5 + canvasHeight * 0.5,
		    mars.x + 5 + canvasHeight * 0.5,
		    mars.y - 5 + canvasHeight * 0.5)

window = Tk()
canvas = Canvas(window, 
           width=canvasWidth, 
           height=canvasHeight)
canvas.create_oval(0 - 10 + canvasHeight * 0.5,
		   0 + 10 + canvasHeight * 0.5,
		   0 + 10 + canvasHeight * 0.5,
		   0 - 10 + canvasHeight * 0.5, fill='yellow')
button = Button(window, text="advance", command=cadvance)
button.pack()
canvas.pack()
mainloop()